package dev.tato.eCommerce.service;

import dev.tato.eCommerce.model.dto.request.auth.RegistrationRequest;

public interface AuthService {

    void register(RegistrationRequest registrationRequest);
}
