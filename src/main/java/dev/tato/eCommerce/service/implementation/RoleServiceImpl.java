package dev.tato.eCommerce.service.implementation;

import dev.tato.eCommerce.model.entity.Role;
import dev.tato.eCommerce.repository.RoleRepository;
import dev.tato.eCommerce.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Override
    public List<Role> fetchByRoleNames(String... roleNames) {
        return roleRepository.findAllByRoleIn(roleNames);
    }

}
