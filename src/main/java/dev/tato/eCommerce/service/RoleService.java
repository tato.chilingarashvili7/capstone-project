package dev.tato.eCommerce.service;

import dev.tato.eCommerce.model.entity.Role;

import java.util.List;

public interface RoleService {

    List<Role> fetchByRoleNames(String... roleNames);

}
