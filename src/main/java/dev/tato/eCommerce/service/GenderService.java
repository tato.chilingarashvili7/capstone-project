package dev.tato.eCommerce.service;

import dev.tato.eCommerce.model.entity.Gender;

import java.util.Map;

public interface GenderService {

    Map<String, Gender> fetchGender();
    Gender fetchGender(String gender);
}
